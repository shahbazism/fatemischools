from django.apps import AppConfig


class DbschoolsConfig(AppConfig):
    name = 'DBSchools'
