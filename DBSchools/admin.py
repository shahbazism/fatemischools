from django.contrib import admin
from .models import School
from .models import Regin
from .models import City
from .models import Provice
from .models import Type
from .models import Grade
from .models import Gender
from .models import Event


# Register your models here.
class SchoolInline(admin.TabularInline):
    model = School
    fields = [
        'sanaad_code',
        'name',
        'gender',

    ]

@admin.register(School)
class School (admin.ModelAdmin):
    list_display= ('name', 'display_type' , 'display_PCR')
    list_filter = ('type', 'gender')


    def display_PCR (self, obj):
        return (obj.city, obj.province, obj.regin)
    def display_type (self, obj):
        return obj.type

@admin.register(Event)
class Event (admin.ModelAdmin):
    list_display = ['event']

@admin.register(Regin)
class Regin (admin.ModelAdmin):
    list_display = ['regin']

@admin.register(City)
class City(admin.ModelAdmin):
    list_display = ['city']

@admin.register(Grade)
class Grade(admin.ModelAdmin):
    list_display = ['grade']

@admin.register(Type)
class Type(admin.ModelAdmin):
    list_display = ['type']
    # inlines = [SchoolInline]

@admin.register(Provice)
class Provice(admin.ModelAdmin):
    list_display = ['provice']

@admin.register(Gender)
class Gender(admin.ModelAdmin):
    list_display = ['gender']