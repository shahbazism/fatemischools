from django.db import models


# Create your models here.

class Grade (models.Model):
    grade = models.CharField(max_length=100)

    def __str__(self):
        return self.grade

class Event (models.Model):
    event = models.CharField(max_length=100)

    def __str__(self):
        return self.event

class School (models.Model):
    sanaad_code = models.CharField( max_length= 8, blank=True, null= True , unique=True)
    name = models.CharField( max_length=200, blank=False, null=False)
    type = models.ForeignKey( 'Type', on_delete=models.SET_NULL, null= True, blank=True)
    gender = models.ForeignKey( 'Gender', on_delete=models.SET_NULL, null = True, blank=True)
    students_number = models.CharField( max_length=10, blank=True, null=True)
    grade = models.ManyToManyField( Grade, blank=True)
    province = models.ForeignKey( 'Provice' , on_delete=models.SET_NULL, null=True , blank=True)
    city = models.ForeignKey( 'City' , on_delete=models.SET_NULL, null=True, blank=True)
    regin = models.ForeignKey( 'Regin' , on_delete=models.SET_NULL, null=True, blank=True)
    level = models.CharField( max_length= 50, blank=True, null=True)
    Telephone = models.CharField( max_length=13 , null=True , blank=True)
    Telephone2 = models.CharField( max_length=13, null=True, blank=True)
    manager_name = models.CharField(max_length=200, null=True , blank=True)
    connector = models.CharField(max_length= 200 , null=True , blank= True)
    mobilephone = models.CharField(max_length=13 , null=True , blank=True)
    address = models.CharField(max_length=500 , null=True , blank=True)
    zip_code = models.CharField(max_length=10 , null=True , blank=True)
    email = models.CharField(max_length= 300 , null= True , blank=True)
    event = models.ManyToManyField(Event , blank=True)


    class Meta:
        ordering = ['regin','name']


    def __str__(self):
        return self.name

class Provice (models.Model):
    provice = models.CharField(max_length=200)

    def __str__(self):
        return self.provice

class City (models.Model):
    city = models.CharField(max_length=200)

    def __str__(self):
        return self.city

class Regin (models.Model):
    regin = models.CharField(max_length=200)

    def __str__(self):
        return self.regin

class Type (models.Model):
    type = models.CharField(max_length=100)

    def __str__(self):
        return self.type

class Gender (models.Model):
    gender = models.CharField(max_length=100)

    def __str__(self):
        return self.gender


# class School