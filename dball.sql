CREATE DATABASE  IF NOT EXISTS `dball` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci */ /*!80016 DEFAULT ENCRYPTION='N' */;
USE `dball`;
-- MySQL dump 10.13  Distrib 8.0.18, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: dball
-- ------------------------------------------------------
-- Server version	8.0.18

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `auth_group`
--

DROP TABLE IF EXISTS `auth_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `auth_group` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(150) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_group`
--

LOCK TABLES `auth_group` WRITE;
/*!40000 ALTER TABLE `auth_group` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_group` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_group_permissions`
--

DROP TABLE IF EXISTS `auth_group_permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `auth_group_permissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `group_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `auth_group_permissions_group_id_permission_id_0cd325b0_uniq` (`group_id`,`permission_id`),
  KEY `auth_group_permissio_permission_id_84c5c92e_fk_auth_perm` (`permission_id`),
  CONSTRAINT `auth_group_permissio_permission_id_84c5c92e_fk_auth_perm` FOREIGN KEY (`permission_id`) REFERENCES `auth_permission` (`id`),
  CONSTRAINT `auth_group_permissions_group_id_b120cbf9_fk_auth_group_id` FOREIGN KEY (`group_id`) REFERENCES `auth_group` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_group_permissions`
--

LOCK TABLES `auth_group_permissions` WRITE;
/*!40000 ALTER TABLE `auth_group_permissions` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_group_permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_permission`
--

DROP TABLE IF EXISTS `auth_permission`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `auth_permission` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `content_type_id` int(11) NOT NULL,
  `codename` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `auth_permission_content_type_id_codename_01ab375a_uniq` (`content_type_id`,`codename`),
  CONSTRAINT `auth_permission_content_type_id_2f476e4b_fk_django_co` FOREIGN KEY (`content_type_id`) REFERENCES `django_content_type` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=57 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_permission`
--

LOCK TABLES `auth_permission` WRITE;
/*!40000 ALTER TABLE `auth_permission` DISABLE KEYS */;
INSERT INTO `auth_permission` VALUES (1,'Can add log entry',1,'add_logentry'),(2,'Can change log entry',1,'change_logentry'),(3,'Can delete log entry',1,'delete_logentry'),(4,'Can view log entry',1,'view_logentry'),(5,'Can add permission',2,'add_permission'),(6,'Can change permission',2,'change_permission'),(7,'Can delete permission',2,'delete_permission'),(8,'Can view permission',2,'view_permission'),(9,'Can add group',3,'add_group'),(10,'Can change group',3,'change_group'),(11,'Can delete group',3,'delete_group'),(12,'Can view group',3,'view_group'),(13,'Can add user',4,'add_user'),(14,'Can change user',4,'change_user'),(15,'Can delete user',4,'delete_user'),(16,'Can view user',4,'view_user'),(17,'Can add content type',5,'add_contenttype'),(18,'Can change content type',5,'change_contenttype'),(19,'Can delete content type',5,'delete_contenttype'),(20,'Can view content type',5,'view_contenttype'),(21,'Can add session',6,'add_session'),(22,'Can change session',6,'change_session'),(23,'Can delete session',6,'delete_session'),(24,'Can view session',6,'view_session'),(25,'Can add city',7,'add_city'),(26,'Can change city',7,'change_city'),(27,'Can delete city',7,'delete_city'),(28,'Can view city',7,'view_city'),(29,'Can add event',8,'add_event'),(30,'Can change event',8,'change_event'),(31,'Can delete event',8,'delete_event'),(32,'Can view event',8,'view_event'),(33,'Can add gender',9,'add_gender'),(34,'Can change gender',9,'change_gender'),(35,'Can delete gender',9,'delete_gender'),(36,'Can view gender',9,'view_gender'),(37,'Can add grade',10,'add_grade'),(38,'Can change grade',10,'change_grade'),(39,'Can delete grade',10,'delete_grade'),(40,'Can view grade',10,'view_grade'),(41,'Can add provice',11,'add_provice'),(42,'Can change provice',11,'change_provice'),(43,'Can delete provice',11,'delete_provice'),(44,'Can view provice',11,'view_provice'),(45,'Can add regin',12,'add_regin'),(46,'Can change regin',12,'change_regin'),(47,'Can delete regin',12,'delete_regin'),(48,'Can view regin',12,'view_regin'),(49,'Can add type',13,'add_type'),(50,'Can change type',13,'change_type'),(51,'Can delete type',13,'delete_type'),(52,'Can view type',13,'view_type'),(53,'Can add school',14,'add_school'),(54,'Can change school',14,'change_school'),(55,'Can delete school',14,'delete_school'),(56,'Can view school',14,'view_school');
/*!40000 ALTER TABLE `auth_permission` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_user`
--

DROP TABLE IF EXISTS `auth_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `auth_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `password` varchar(128) NOT NULL,
  `last_login` datetime(6) DEFAULT NULL,
  `is_superuser` tinyint(1) NOT NULL,
  `username` varchar(150) NOT NULL,
  `first_name` varchar(30) NOT NULL,
  `last_name` varchar(150) NOT NULL,
  `email` varchar(254) NOT NULL,
  `is_staff` tinyint(1) NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `date_joined` datetime(6) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_user`
--

LOCK TABLES `auth_user` WRITE;
/*!40000 ALTER TABLE `auth_user` DISABLE KEYS */;
INSERT INTO `auth_user` VALUES (1,'pbkdf2_sha256$180000$pzzsBf4D4xR4$BOZ1CT8lmeCgTaZvUY68HRR6rFdZHWYSpzLfsgnPY2M=','2019-12-25 08:16:11.606346',1,'saeedshahbazi','محمدسعید','شهبازی','arash9775@gmail.com',1,1,'2019-12-23 09:04:08.000000'),(2,'pbkdf2_sha256$180000$VCFkSLkc04hN$LBGY92JhD8PpUftMRGWeliPX/UQK+Tscp7GY0rI06aI=','2019-12-23 09:18:38.622264',1,'aaasss','اکبر','جوجه','',1,1,'2019-12-23 09:17:03.000000');
/*!40000 ALTER TABLE `auth_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_user_groups`
--

DROP TABLE IF EXISTS `auth_user_groups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `auth_user_groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `auth_user_groups_user_id_group_id_94350c0c_uniq` (`user_id`,`group_id`),
  KEY `auth_user_groups_group_id_97559544_fk_auth_group_id` (`group_id`),
  CONSTRAINT `auth_user_groups_group_id_97559544_fk_auth_group_id` FOREIGN KEY (`group_id`) REFERENCES `auth_group` (`id`),
  CONSTRAINT `auth_user_groups_user_id_6a12ed8b_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_user_groups`
--

LOCK TABLES `auth_user_groups` WRITE;
/*!40000 ALTER TABLE `auth_user_groups` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_user_groups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_user_user_permissions`
--

DROP TABLE IF EXISTS `auth_user_user_permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `auth_user_user_permissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `auth_user_user_permissions_user_id_permission_id_14a6b632_uniq` (`user_id`,`permission_id`),
  KEY `auth_user_user_permi_permission_id_1fbb5f2c_fk_auth_perm` (`permission_id`),
  CONSTRAINT `auth_user_user_permi_permission_id_1fbb5f2c_fk_auth_perm` FOREIGN KEY (`permission_id`) REFERENCES `auth_permission` (`id`),
  CONSTRAINT `auth_user_user_permissions_user_id_a95ead1b_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_user_user_permissions`
--

LOCK TABLES `auth_user_user_permissions` WRITE;
/*!40000 ALTER TABLE `auth_user_user_permissions` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_user_user_permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dbschools_city`
--

DROP TABLE IF EXISTS `dbschools_city`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `dbschools_city` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `city` varchar(200) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dbschools_city`
--

LOCK TABLES `dbschools_city` WRITE;
/*!40000 ALTER TABLE `dbschools_city` DISABLE KEYS */;
INSERT INTO `dbschools_city` VALUES (1,'asdasd');
/*!40000 ALTER TABLE `dbschools_city` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dbschools_event`
--

DROP TABLE IF EXISTS `dbschools_event`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `dbschools_event` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `event` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dbschools_event`
--

LOCK TABLES `dbschools_event` WRITE;
/*!40000 ALTER TABLE `dbschools_event` DISABLE KEYS */;
INSERT INTO `dbschools_event` VALUES (1,'fwerd');
/*!40000 ALTER TABLE `dbschools_event` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dbschools_gender`
--

DROP TABLE IF EXISTS `dbschools_gender`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `dbschools_gender` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `gender` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dbschools_gender`
--

LOCK TABLES `dbschools_gender` WRITE;
/*!40000 ALTER TABLE `dbschools_gender` DISABLE KEYS */;
INSERT INTO `dbschools_gender` VALUES (1,'asdasd');
/*!40000 ALTER TABLE `dbschools_gender` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dbschools_grade`
--

DROP TABLE IF EXISTS `dbschools_grade`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `dbschools_grade` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `grade` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dbschools_grade`
--

LOCK TABLES `dbschools_grade` WRITE;
/*!40000 ALTER TABLE `dbschools_grade` DISABLE KEYS */;
INSERT INTO `dbschools_grade` VALUES (1,'asdasda'),(2,'rgrgrg');
/*!40000 ALTER TABLE `dbschools_grade` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dbschools_provice`
--

DROP TABLE IF EXISTS `dbschools_provice`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `dbschools_provice` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `provice` varchar(200) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dbschools_provice`
--

LOCK TABLES `dbschools_provice` WRITE;
/*!40000 ALTER TABLE `dbschools_provice` DISABLE KEYS */;
INSERT INTO `dbschools_provice` VALUES (1,'asdsad'),(2,'rgrggrg');
/*!40000 ALTER TABLE `dbschools_provice` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dbschools_regin`
--

DROP TABLE IF EXISTS `dbschools_regin`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `dbschools_regin` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `regin` varchar(200) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dbschools_regin`
--

LOCK TABLES `dbschools_regin` WRITE;
/*!40000 ALTER TABLE `dbschools_regin` DISABLE KEYS */;
INSERT INTO `dbschools_regin` VALUES (1,'asdrawd'),(2,'sdgfsf');
/*!40000 ALTER TABLE `dbschools_regin` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dbschools_school`
--

DROP TABLE IF EXISTS `dbschools_school`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `dbschools_school` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sanaad_code` varchar(8) DEFAULT NULL,
  `name` varchar(200) NOT NULL,
  `students_number` varchar(10) DEFAULT NULL,
  `level` varchar(50) DEFAULT NULL,
  `Telephone` varchar(13) DEFAULT NULL,
  `Telephone2` varchar(13) DEFAULT NULL,
  `manager_name` varchar(200) DEFAULT NULL,
  `connector` varchar(200) DEFAULT NULL,
  `mobilephone` varchar(13) DEFAULT NULL,
  `address` varchar(500) DEFAULT NULL,
  `zip_code` varchar(10) DEFAULT NULL,
  `email` varchar(300) DEFAULT NULL,
  `city_id` int(11) DEFAULT NULL,
  `gender_id` int(11) DEFAULT NULL,
  `province_id` int(11) DEFAULT NULL,
  `regin_id` int(11) DEFAULT NULL,
  `type_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `sanaad_code` (`sanaad_code`),
  KEY `DBSchools_school_city_id_1fa5d68f_fk_DBSchools_city_id` (`city_id`),
  KEY `DBSchools_school_gender_id_4f532469_fk_DBSchools_gender_id` (`gender_id`),
  KEY `DBSchools_school_province_id_45865b37_fk_DBSchools_provice_id` (`province_id`),
  KEY `DBSchools_school_regin_id_58b841aa_fk_DBSchools_regin_id` (`regin_id`),
  KEY `DBSchools_school_type_id_85894011_fk_DBSchools_type_id` (`type_id`),
  CONSTRAINT `DBSchools_school_city_id_1fa5d68f_fk_DBSchools_city_id` FOREIGN KEY (`city_id`) REFERENCES `dbschools_city` (`id`),
  CONSTRAINT `DBSchools_school_gender_id_4f532469_fk_DBSchools_gender_id` FOREIGN KEY (`gender_id`) REFERENCES `dbschools_gender` (`id`),
  CONSTRAINT `DBSchools_school_province_id_45865b37_fk_DBSchools_provice_id` FOREIGN KEY (`province_id`) REFERENCES `dbschools_provice` (`id`),
  CONSTRAINT `DBSchools_school_regin_id_58b841aa_fk_DBSchools_regin_id` FOREIGN KEY (`regin_id`) REFERENCES `dbschools_regin` (`id`),
  CONSTRAINT `DBSchools_school_type_id_85894011_fk_DBSchools_type_id` FOREIGN KEY (`type_id`) REFERENCES `dbschools_type` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dbschools_school`
--

LOCK TABLES `dbschools_school` WRITE;
/*!40000 ALTER TABLE `dbschools_school` DISABLE KEYS */;
INSERT INTO `dbschools_school` VALUES (1,'21214','asda','asdasd','sdrgr','rgfdfa','fdaserd','efrwef','erfwetr','ertwerw','erwerwe','werhtghg','erweref',1,1,1,1,1),(2,'ttg','rgr',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,2,1,1);
/*!40000 ALTER TABLE `dbschools_school` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dbschools_school_event`
--

DROP TABLE IF EXISTS `dbschools_school_event`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `dbschools_school_event` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `school_id` int(11) NOT NULL,
  `event_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `DBSchools_school_event_school_id_event_id_45dd5185_uniq` (`school_id`,`event_id`),
  KEY `DBSchools_school_event_event_id_bd1ec041_fk_DBSchools_event_id` (`event_id`),
  CONSTRAINT `DBSchools_school_event_event_id_bd1ec041_fk_DBSchools_event_id` FOREIGN KEY (`event_id`) REFERENCES `dbschools_event` (`id`),
  CONSTRAINT `DBSchools_school_event_school_id_d8650d10_fk_DBSchools_school_id` FOREIGN KEY (`school_id`) REFERENCES `dbschools_school` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dbschools_school_event`
--

LOCK TABLES `dbschools_school_event` WRITE;
/*!40000 ALTER TABLE `dbschools_school_event` DISABLE KEYS */;
INSERT INTO `dbschools_school_event` VALUES (1,1,1);
/*!40000 ALTER TABLE `dbschools_school_event` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dbschools_school_grade`
--

DROP TABLE IF EXISTS `dbschools_school_grade`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `dbschools_school_grade` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `school_id` int(11) NOT NULL,
  `grade_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `DBSchools_school_grade_school_id_grade_id_9a754267_uniq` (`school_id`,`grade_id`),
  KEY `DBSchools_school_grade_grade_id_f2207642_fk_DBSchools_grade_id` (`grade_id`),
  CONSTRAINT `DBSchools_school_grade_grade_id_f2207642_fk_DBSchools_grade_id` FOREIGN KEY (`grade_id`) REFERENCES `dbschools_grade` (`id`),
  CONSTRAINT `DBSchools_school_grade_school_id_e4377723_fk_DBSchools_school_id` FOREIGN KEY (`school_id`) REFERENCES `dbschools_school` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dbschools_school_grade`
--

LOCK TABLES `dbschools_school_grade` WRITE;
/*!40000 ALTER TABLE `dbschools_school_grade` DISABLE KEYS */;
INSERT INTO `dbschools_school_grade` VALUES (1,1,1),(2,2,2);
/*!40000 ALTER TABLE `dbschools_school_grade` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dbschools_type`
--

DROP TABLE IF EXISTS `dbschools_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `dbschools_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dbschools_type`
--

LOCK TABLES `dbschools_type` WRITE;
/*!40000 ALTER TABLE `dbschools_type` DISABLE KEYS */;
INSERT INTO `dbschools_type` VALUES (1,'dasd');
/*!40000 ALTER TABLE `dbschools_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_admin_log`
--

DROP TABLE IF EXISTS `django_admin_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `django_admin_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `action_time` datetime(6) NOT NULL,
  `object_id` longtext,
  `object_repr` varchar(200) NOT NULL,
  `action_flag` smallint(5) unsigned NOT NULL,
  `change_message` longtext NOT NULL,
  `content_type_id` int(11) DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `django_admin_log_content_type_id_c4bce8eb_fk_django_co` (`content_type_id`),
  KEY `django_admin_log_user_id_c564eba6_fk_auth_user_id` (`user_id`),
  CONSTRAINT `django_admin_log_content_type_id_c4bce8eb_fk_django_co` FOREIGN KEY (`content_type_id`) REFERENCES `django_content_type` (`id`),
  CONSTRAINT `django_admin_log_user_id_c564eba6_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`),
  CONSTRAINT `django_admin_log_chk_1` CHECK ((`action_flag` >= 0))
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_admin_log`
--

LOCK TABLES `django_admin_log` WRITE;
/*!40000 ALTER TABLE `django_admin_log` DISABLE KEYS */;
INSERT INTO `django_admin_log` VALUES (1,'2019-12-23 09:05:53.410012','1','dasd',1,'[{\"added\": {}}]',13,1),(2,'2019-12-23 09:05:55.704477','1','asdasd',1,'[{\"added\": {}}]',9,1),(3,'2019-12-23 09:06:00.666814','1','asdasda',1,'[{\"added\": {}}]',10,1),(4,'2019-12-23 09:06:05.197169','1','asdsad',1,'[{\"added\": {}}]',11,1),(5,'2019-12-23 09:06:07.634357','1','asdasd',1,'[{\"added\": {}}]',7,1),(6,'2019-12-23 09:06:11.179344','1','asdrawd',1,'[{\"added\": {}}]',12,1),(7,'2019-12-23 09:06:26.737253','1','fwerd',1,'[{\"added\": {}}]',8,1),(8,'2019-12-23 09:06:28.593396','1','asda',1,'[{\"added\": {}}]',14,1),(9,'2019-12-23 09:14:08.287467','1','saeedshahbazi',2,'[{\"changed\": {\"fields\": [\"First name\", \"Last name\"]}}]',4,1),(10,'2019-12-23 09:14:37.699092','2','rgrgrg',1,'[{\"added\": {}}]',10,1),(11,'2019-12-23 09:14:40.670314','2','rgrggrg',1,'[{\"added\": {}}]',11,1),(12,'2019-12-23 09:14:45.939006','2','rgr',1,'[{\"added\": {}}]',14,1),(13,'2019-12-23 09:17:04.000117','2','aaasss',1,'[{\"added\": {}}]',4,1),(14,'2019-12-23 09:17:31.134982','2','aaasss',2,'[{\"changed\": {\"fields\": [\"First name\", \"Last name\", \"Staff status\", \"Superuser status\"]}}]',4,1),(15,'2019-12-23 09:18:46.865275','2','sdgfsf',1,'[{\"added\": {}}]',12,2);
/*!40000 ALTER TABLE `django_admin_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_content_type`
--

DROP TABLE IF EXISTS `django_content_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `django_content_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `app_label` varchar(100) NOT NULL,
  `model` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `django_content_type_app_label_model_76bd3d3b_uniq` (`app_label`,`model`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_content_type`
--

LOCK TABLES `django_content_type` WRITE;
/*!40000 ALTER TABLE `django_content_type` DISABLE KEYS */;
INSERT INTO `django_content_type` VALUES (1,'admin','logentry'),(3,'auth','group'),(2,'auth','permission'),(4,'auth','user'),(5,'contenttypes','contenttype'),(7,'DBSchools','city'),(8,'DBSchools','event'),(9,'DBSchools','gender'),(10,'DBSchools','grade'),(11,'DBSchools','provice'),(12,'DBSchools','regin'),(14,'DBSchools','school'),(13,'DBSchools','type'),(6,'sessions','session');
/*!40000 ALTER TABLE `django_content_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_migrations`
--

DROP TABLE IF EXISTS `django_migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `django_migrations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `app` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `applied` datetime(6) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_migrations`
--

LOCK TABLES `django_migrations` WRITE;
/*!40000 ALTER TABLE `django_migrations` DISABLE KEYS */;
INSERT INTO `django_migrations` VALUES (1,'DBSchools','0001_initial','2019-12-23 09:02:45.495088'),(2,'contenttypes','0001_initial','2019-12-23 09:03:05.950784'),(3,'auth','0001_initial','2019-12-23 09:03:09.365608'),(4,'admin','0001_initial','2019-12-23 09:03:23.959398'),(5,'admin','0002_logentry_remove_auto_add','2019-12-23 09:03:27.755500'),(6,'admin','0003_logentry_add_action_flag_choices','2019-12-23 09:03:27.923213'),(7,'contenttypes','0002_remove_content_type_name','2019-12-23 09:03:30.380124'),(8,'auth','0002_alter_permission_name_max_length','2019-12-23 09:03:32.411971'),(9,'auth','0003_alter_user_email_max_length','2019-12-23 09:03:32.980622'),(10,'auth','0004_alter_user_username_opts','2019-12-23 09:03:33.304434'),(11,'auth','0005_alter_user_last_login_null','2019-12-23 09:03:34.686859'),(12,'auth','0006_require_contenttypes_0002','2019-12-23 09:03:34.997239'),(13,'auth','0007_alter_validators_add_error_messages','2019-12-23 09:03:35.227099'),(14,'auth','0008_alter_user_username_max_length','2019-12-23 09:03:36.920612'),(15,'auth','0009_alter_user_last_name_max_length','2019-12-23 09:03:38.415269'),(16,'auth','0010_alter_group_name_max_length','2019-12-23 09:03:38.662805'),(17,'auth','0011_update_proxy_permissions','2019-12-23 09:03:38.796720'),(18,'sessions','0001_initial','2019-12-23 09:03:39.424046');
/*!40000 ALTER TABLE `django_migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_session`
--

DROP TABLE IF EXISTS `django_session`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `django_session` (
  `session_key` varchar(40) NOT NULL,
  `session_data` longtext NOT NULL,
  `expire_date` datetime(6) NOT NULL,
  PRIMARY KEY (`session_key`),
  KEY `django_session_expire_date_a5c62663` (`expire_date`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_session`
--

LOCK TABLES `django_session` WRITE;
/*!40000 ALTER TABLE `django_session` DISABLE KEYS */;
INSERT INTO `django_session` VALUES ('mku2rpxxo0k82usjzblmlqmimn9z5qsw','Nzc4YmRhYWEzZDY0ZTg0NGNlMDhmM2ZkYWI1ZDJlYWE1NjhkOGY3NTp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiJmZmI3NTE2MTZmODE1ZTA2NTgyNmFjOTdjMDRhNTBiM2VlMWUwZDlhIn0=','2020-01-08 08:16:11.687298'),('u0umz796iwvnvof9bzb6nbxaks2zne6a','ZmRlZTZjZTJkMmJlMGZkODVhMWRkZTFkNzA4YmQ4NzNjNTk0MTExMDp7Il9hdXRoX3VzZXJfaWQiOiIyIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiIwNjhlZjhmOWU3YzQ2ZGM4OWIxOGZhYTA3YWU2NzA1MWY2NGMwY2Q5In0=','2020-01-06 09:18:38.744188');
/*!40000 ALTER TABLE `django_session` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-12-25 12:30:07
